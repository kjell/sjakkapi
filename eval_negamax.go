package main

import (
	"log"
	"math/rand"

	"github.com/notnil/chess"
)

// negaMax, as described on https://www.chessprogramming.org/Negamax

// int negaMax( int depth ) {
//     if ( depth == 0 ) return evaluate();
//     int max = -oo;
//     for ( all moves)  {
//         score = -negaMax( depth - 1 );
//         if( score > max )
//             max = score;
//     }
//     return max;
// }

// negaMaxFromFen is the root negamax function which calls negaMax on
// all posible moves from current position
func negaMaxFromFen(f string, depth int) (*Move, error) {
	fen, err := chess.FEN(f)
	if err != nil {
		return nil, err
	}
	game := chess.NewGame(chess.UseNotation(chess.LongAlgebraicNotation{}), fen)
	if game.Outcome() != chess.NoOutcome {
		return &Move{"", "", f, string(game.Outcome()), game.Method().String(), -999, nil}, nil
	}
	// black want a large negative number
	// white want a large positive number
	color := 1
	p := game.Position()
	if p.Turn() == chess.Black {
		color = -1
	}

	var moves = p.ValidMoves()
	//	shuffle so we dont do the same move everytime (if we have several positions with the same score)
	dest := make([]*chess.Move, len(moves))
	perm := rand.Perm(len(moves))
	for i, v := range perm {
		dest[v] = moves[i]
	}
	moves = dest
	// -- shuffle

	bestScore := -10000
	var bestMove *chess.Move
	for _, m := range moves {
		g := p.Update(m)

		v := -negaMax(g, depth, color*-1)
		if v > bestScore {
			bestScore = v
			bestMove = m
		}
	}

	game.Move(bestMove)

	// get score from Stockfish
	s, err := getScore(10, f)
	if err != nil {
		// http://localhost:3001/api/nextmove?FEN=rnbqkbnN/3p4/8/1BP1pP2/p7/8/PPP2PPP/RNBQ1RK1%20b%20kq%20-%200%2010&move=a8a6&movetype=minmax
		log.Println(f)
		log.Println("SF not happy:", err)
	}

	s1 := bestMove.S1().String()
	s2 := bestMove.S2().String()
	if bestMove == nil {
		log.Fatal("ups")
	}
	return &Move{s1, s2, f, string(game.Outcome()), game.Method().String(), bestScore, s}, nil

}

func negaMax(game *chess.Position, depth int, color int) int {
	if depth == 0 {
		v := evaluateBoard(game.Board())
		return v * color
	}

	var moves = game.ValidMoves()
	bestScore := -9999

	if len(moves) == 0 {

		// In order for negaMax to work, your Static
		// Evaluation function must return a score
		// relative to the side to being evaluated
		return bestScore * color
	}

	for _, m := range moves {
		g := game.Update(m)

		nextScore := -negaMax(g, depth-1, color*-1)
		//		log.Println("debug", depth, bestScore, nextScore, factor, m)
		if nextScore > bestScore {
			bestScore = nextScore
		}
	}

	return bestScore
}
