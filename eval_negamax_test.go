package main

import (
	"testing"
)

func Test_negaMaxFromFen(t *testing.T) {
	type args struct {
		f     string
		depth int
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			"sane move depth 0",
			args{"rnbqk2r/pppp1ppp/8/2b1p3/3Pn3/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 0 1", 0},
			[]string{"d4c5", "c3e4"},
			false,
		},
		{
			"sane move depth 1",
			args{"rnbqk2r/pppp1ppp/8/2b1p3/3Pn3/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 0 1", 1},
			[]string{"d4c5", "c3e4"},
			false,
		},
		{
			"sane move depth 2",
			args{"rnbqk2r/pppp1ppp/8/2b1p3/3Pn3/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 0 1", 2},
			[]string{"d4c5", "c3e4"},
			false,
		},
		{
			"sane black move (take queen)",
			args{"rnb1kbr1/ppp1n2p/3P1P2/3P4/6Q1/2N5/PPP2P1P/R1B1KBNR b KQq - 0 10", 0},
			[]string{"g8g4", "c8g4"},
			false,
		},
		{
			"sane black move (take queen)",
			args{"rnb1kbr1/ppp1n2p/3P1P2/3P4/6Q1/2N5/PPP2P1P/R1B1KBNR b KQq - 0 10", 1},
			[]string{"g8g4", "c8g4"},
			false,
		},
		{
			"sane black move (take queen)",
			args{"rnb1kbr1/ppp1n2p/3P1P2/3P4/6Q1/2N5/PPP2P1P/R1B1KBNR b KQq - 0 10", 2},
			[]string{"g8g4", "c8g4"},
			false,
		},
		{
			"sane white move",
			args{"rnbqkbnr/pp3p2/3pp1pp/2p5/1P1PP3/2PB4/P4PPP/RNBQK1NR w KQkq - 0 5", 2},
			[]string{"d4c5", "b4c5"},
			false,
		},
		{
			"black check. Only one legal move",
			args{"rnb5/2p2Qkp/p2pr2p/1p6/8/P1P1P1P1/2PN3P/5RK1 b - - 1 21", 2},
			[]string{"g7h8"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := negaMaxFromFen(tt.args.f, tt.args.depth)
			if (err != nil) != tt.wantErr {
				t.Errorf("negaMaxFromFen() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			success := false
			for _, m := range tt.want {
				if m == got.String() {
					success = true
				}
			}

			if !success {
				t.Errorf("Expected %s, got %s", tt.want, got)
				logDebugLink(tt.args.f)
			}
		})
	}
}
