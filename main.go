package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"

	"github.com/notnil/chess"
	"gopkg.in/freeeve/uci.v1"
)

var port = "3001"

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {

	if len(os.Args) > 1 && os.Args[1] == "profile" {
		profileNegaMax()
		return
	}

	// static
	fs := http.StripPrefix("/web/", http.FileServer(http.Dir("./web")))
	http.Handle("/web/", fs)

	// api

	// given url parameters FEN and move
	// the move will be applyed and new FEN is generated.
	// also, move suggestions will be added as well as a basic
	// evaluation from stockfish

	// see type Move
	http.HandleFunc("/api/nextmove", nextMoveHandler)

	// return valid moved given a valid FEN
	http.HandleFunc("/api/legalmoves", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		FEN := r.URL.Query().Get("FEN")
		lm, err := legalMovesFromFen(FEN, "")
		if err != nil {
			errorHandler(w, r, http.StatusBadRequest, fmt.Sprint(err))
			return
		}

		enc := json.NewEncoder(w)
		enc.Encode(lm)
	})

	log.Println("starting http server on port", port)
	err := http.ListenAndServe(":"+port, nil)
	log.Fatal(err)
}

// Move contains iformation on current posistion, such as outcome, suggested move and score.
type Move struct {
	From        string `json:"from"`
	To          string `json:"to"`
	FEN         string // current FEN
	Outcome     string
	Method      string
	Simplescore int `json:"simplescore"`
	UCI         *uci.Results
}

func (m *Move) String() string {
	return fmt.Sprintf("%s%s", m.From, m.To)
}

// Suggest a random legal move from FEN
func randomMoveFromFen(f string) (*Move, error) {

	fen, err := chess.FEN(f)
	if err != nil {
		return nil, err
	}
	game := chess.NewGame(chess.UseNotation(chess.LongAlgebraicNotation{}), fen)
	if game.Outcome() != chess.NoOutcome {
		return &Move{"", "", f, string(game.Outcome()), game.Method().String(), -999, nil}, nil
	}
	moves := game.ValidMoves()
	move := moves[rand.Intn(len(moves))]

	s1 := move.S1().String()
	s2 := move.S2().String()

	game.Move(move)

	s, err := getScore(10, f)
	if err != nil {
		log.Fatal(err)
	}
	simplescore := evaluateBoard(game.Position().Board())
	return &Move{s1, s2, f, string(game.Outcome()), game.Method().String(), simplescore, s}, nil

}

// go build &&
// go tool pprof -http=:8080 profile /tmp/profile617754421/cpu.pprof
func profileNegaMax() {
	//	defer profile.Start().Stop()

	m, err := negaMaxFromFen("rnbqk2r/pppp1ppp/8/2b1p3/3Pn3/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 0 1", 3)
	if err != nil {
		log.Fatal(err)
	}

	if m.String() != "d4c5" {
		log.Println("Did not get expected move d4c5. got ", m)
	}
}

//
// Eksempler:
//
// ett mulig trekk
// http://localhost:3001/api/legalmoves?FEN=rnbN1b1r/1pp1kQ1p/p2p1n2/5p2/P4P2/4P3/1PPP2PP/RNB1KB1R%20b%20KQ%20-%201%209
//
// matt
// http://localhost:3001/api/nextmove?FEN=r2k1b1r/1ppb1Q1p/p7/1B1N1p2/P4P2/4P3/1PPP1nPP/R1B1K2R%20w%20KQ%20-%203%2015&move=f7d7
//
// promote to queen (a2a1r)
// http://localhost:3001/api/nextmove?FEN=2rk1b1r/1ppb1Q1p/P7/5p2/5P2/2P1P3/pP1P1nPP/2B1K2R%20b%20K%20-%200%2020&move=a2a1r
