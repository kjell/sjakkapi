package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func errorHandler(w http.ResponseWriter, r *http.Request, status int, message string) {
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	enc.Encode(struct {
		HTTPStatus string `json:"httpstatus"`
		Message    string `json:"message"`
	}{
		http.StatusText(status),
		message})
}

var nextMoveHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	log.Println(r.URL)

	if len(r.URL.Query()["FEN"]) != 1 || len(r.URL.Query()["move"]) != 1 {
		errorHandler(w, r, http.StatusBadRequest, "Expected parameters FEN and move")
		return
	}
	prevFEN := r.URL.Query()["FEN"][0]
	move := r.URL.Query()["move"][0]
	current, err := fenFromMove(prevFEN, move)
	if err != nil {
		log.Println(err)
		fmt.Fprintf(w, "%s", err)
		return
	}
	var m *Move
	movetype := "random"
	if len(r.URL.Query()["movetype"]) > 0 {
		movetype = r.URL.Query()["movetype"][0]
	}

	switch movetype {
	case "sane":
		m, err = saneMoveFromFen(current.FEN)
	case "negamax":
		m, err = negaMaxFromFen(current.FEN, 2)
	default:
		m, err = randomMoveFromFen(current.FEN)
	}
	if err != nil {
		log.Println(err)
		fmt.Fprintf(w, "%s", err)
		return
	}
	m.Outcome = current.Outcome.String()
	enc := json.NewEncoder(w)
	enc.Encode(m)
}
