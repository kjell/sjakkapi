package main

import (
	"log"
	"math/rand"

	"github.com/notnil/chess"
)

func saneMoveFromFen(f string) (*Move, error) {
	fen, err := chess.FEN(f)
	if err != nil {
		return nil, err
	}
	game := chess.NewGame(chess.UseNotation(chess.LongAlgebraicNotation{}), fen)
	if game.Outcome() != chess.NoOutcome {
		return &Move{"", "", f, string(game.Outcome()), game.Method().String(), -999, nil}, nil
	}
	bestMove, bestScore := getSaneMove(game.Position())

	game.Move(bestMove)

	// get score from Stockfish
	s, err := getScore(10, f)
	if err != nil {
		log.Fatal(err)
	}
	s1 := bestMove.S1().String()
	s2 := bestMove.S2().String()

	return &Move{s1, s2, f, string(game.Outcome()), game.Method().String(), bestScore, s}, nil

}

func getSaneMove(game *chess.Position) (*chess.Move, int) {
	// black want a large negative number
	// white want a large positive number
	factor := 1
	if game.Turn() == chess.Black {
		factor = -1
	}

	moves := game.ValidMoves()
	//	shuffle so we dont do the same move everytime (if we have several positions with the same score)
	dest := make([]*chess.Move, len(moves))
	perm := rand.Perm(len(moves))
	for i, v := range perm {
		dest[v] = moves[i]
	}
	moves = dest
	// -- shuffle

	bestValue := -9999
	bestScore := 9999 * factor
	var bestMove *chess.Move
	for _, m := range moves {
		g := game.Update(m)

		score := evaluateBoard(g.Board())

		// ev evaluation. positive numbers
		ev := score * factor

		if ev > bestValue {
			bestValue = ev
			bestMove = m
			bestScore = score
		}
	}
	return bestMove, bestScore
}
