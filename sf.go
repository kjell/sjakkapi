package main

import (
	"gopkg.in/freeeve/uci.v1"
)

func getScore(depth int, fen string) (*uci.Results, error) {

	eng, err := uci.NewEngine("/usr/games/stockfish")
	if err != nil {
		return nil, err
	}

	// set some engine options
	eng.SetOptions(uci.Options{
		Hash:    128,
		Ponder:  false,
		OwnBook: true,
		MultiPV: 4,
	})

	// set the starting position
	eng.SetFEN(fen)

	// set some result filter options
	resultOpts := uci.HighestDepthOnly | uci.IncludeUpperbounds | uci.IncludeLowerbounds
	results, err := eng.GoDepth(depth, resultOpts)
	if err != nil {
		return nil, err
	}

	return results, nil
}
