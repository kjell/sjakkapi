package main

import (
	"fmt"
	"log"
	"net/url"

	"github.com/notnil/chess"
)

// f fen as string
// m move as string (a3a4)
type moveResult struct {
	FEN     string
	Outcome chess.Outcome
	Method  string
}

// fenFromMove returns moveResults which contains the new
// board setup (FEN) after the move(m) is done
func fenFromMove(f, m string) (*moveResult, error) {

	game, err := gameAfterMove(f, m)
	if err != nil {
		return nil, err
	}

	return &moveResult{game.FEN(), game.Outcome(), game.Method().String()}, nil
}

// returns a chess.Game after a move ( see fenFromMove )
func gameAfterMove(f, m string) (*chess.Game, error) {
	fen, err := chess.FEN(f)
	if err != nil {
		return nil, err
	}
	game := chess.NewGame(chess.UseNotation(chess.LongAlgebraicNotation{}), fen)
	if m == "" {
		return game, nil
	}

	err = game.MoveStr(m)
	if err != nil {
		return nil, err
	}
	return game, nil
}

// Square represents a coordinate on the chessboard
type Square string

// LegalMoves represents all legal moves in current position.
type LegalMoves map[Square][]Square

func legalMovesFromFen(f, m string) (LegalMoves, error) {
	game, err := gameAfterMove(f, m)
	if err != nil {
		return nil, err
	}

	lm := game.ValidMoves()

	moves := make(LegalMoves)
	for i := 0; i < len(lm); i++ {
		s1 := Square(lm[i].S1().String())
		s2 := Square(lm[i].S2().String())
		moves[s1] = append(moves[s1], s2)
	}

	return moves, nil
}

var pieceValues = map[string]int{
	"k": 900, //king
	"q": 90,  //queen
	"r": 50,  //rook
	"n": 30,  //knight
	"b": 30,  //bishop
	"":  10,  // pawn
}

func evaluateBoard(b *chess.Board) (totalEvaluation int) {

	// SquareMap is a map[Square]Piece
	// for now, square is omitted, but might be hande later if position on board should be concidered in evaluation

	// b.SquareMap() is the bottleneck now. 64% of all.

	for _, p := range b.SquareMap() {
		// log.Printf("%s|%s|%s|%d|%d\n", i, p.Color(), p.Type(), pieceValues[p.Type().String()], totalEvaluation)

		factor := 1
		if p.Color() == chess.Black {
			factor = -1
		}
		totalEvaluation += pieceValues[p.Type().String()] * factor
	}

	return totalEvaluation
}

func evaluateFEN(f string) (int, error) {
	game, err := gameAfterMove(f, "")
	if err != nil {
		return 0, err
	}

	return evaluateBoard(game.Position().Board()), err
}

func logDebugLink(f string) {
	log.Println(f)
	url := fmt.Sprintf("https://lichess.org/analysis?fen=%s", url.QueryEscape(f))
	log.Println(url)
}
