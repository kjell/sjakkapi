package main

import (
	"fmt"
	"testing"

	"github.com/notnil/chess"
)

func Test_fenFromMove(t *testing.T) {
	type args struct {
		fen  string
		move string
	}
	tests := []struct {
		name    string
		args    args
		want    moveResult
		wantErr bool
	}{ // test trekk som fører til matt.
		// test trekk som fører til ny brikke.
		{"hestetrekk",
			args{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", "b1c3"},
			moveResult{"rnbqkbnr/pppppppp/8/8/8/2N5/PPPPPPPP/R1BQKBNR b KQkq - 1 1", "", ""},
			false,
		},
		{"åpning",
			args{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", ""},
			moveResult{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", "", ""},
			false,
		},
		{"mate",
			args{"r2k1b1r/1ppb1Q1p/p7/1B1N1p2/P4P2/4P3/1PPP1nPP/R1B1K2R w KQ - 3 15", "f7d7"},
			moveResult{"r2k1b1r/1ppQ3p/p7/1B1N1p2/P4P2/4P3/1PPP1nPP/R1B1K2R b KQ - 0 15", "", ""},
			false,
		},
		{"promote",
			args{"2rk1b1r/1ppb1Q1p/P7/5p2/5P2/2P1P3/pP1P1nPP/2B1K2R b K - 0 20", "a2a1q"},
			moveResult{"2rk1b1r/1ppb1Q1p/P7/5p2/5P2/2P1P3/1P1P1nPP/q1B1K2R w K - 0 21", "", ""},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fenFromMove(tt.args.fen, tt.args.move)
			if (err != nil) != tt.wantErr {
				t.Errorf("fenFromMove() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.FEN != tt.want.FEN {
				t.Errorf("fenFromMove() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_legalMovesFromFen(t *testing.T) {
	type args struct {
		f string
		m string
	}
	tests := []struct {
		name    string
		args    args
		want    LegalMoves
		wantErr bool
	}{
		{
			"opening",
			args{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", ""},
			LegalMoves{"a2": []Square{"a3", "a4"}},
			false,
		},
		{
			"mate",
			args{"r2k1b1r/1ppQ3p/p7/1B1N1p2/P4P2/4P3/1PPP1nPP/R1B1K2R b KQ - 0 15", ""},
			LegalMoves{"a2": []Square{}}, // no legal moves
			false,
		},
		/*		{
				"mate",
				args{"2rk1b1r/1ppb1Q1p/P7/5p2/5P2/2P1P3/pP1P1nPP/2B1K2R b K - 0 20", ""},
				LegalMoves{"a2": []Square{"a1q", "a1r", "a1b", "a1n"}}, // no legal moves
				false,
			},*/
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := legalMovesFromFen(tt.args.f, tt.args.m)
			if (err != nil) != tt.wantErr {
				t.Errorf("legalMovesFromFen() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			for k := range tt.want {
				if len(got[k]) != len(tt.want[k]) {
					t.Errorf("legalMovesFromFen() size = %d, want %d", len(got[k]), len(tt.want[k]))
					return
				}
				for i := range got[k] {
					if got[k][i] != tt.want[k][i] {
						t.Errorf("legalMovesFromFen() = %v, want %v", got[k][i], tt.want[k][i])
					}
				}
			}
		})
	}
}

func Test_evaluateFEN(t *testing.T) {
	type args struct {
		f string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{"start position",
			args{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"},
			0,
			false,
		},
		{"missing white pawn",
			args{"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPP1/RNBQKBNR w KQkq - 0 1"},
			-10,
			false,
		},
		{"missing black pawn",
			args{"rnbqkbnr/ppppppp1/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"},
			10,
			false,
		},
		{"random position",
			args{"rnb1kbr1/ppp1n2p/3P1P2/3P4/6Q1/2N5/PPP2P1P/R1B1KBNR b KQq - 0 10"},
			130,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := evaluateFEN(tt.args.f)
			if (err != nil) != tt.wantErr {
				t.Errorf("evaluateFEN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("evaluateFEN() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sanemove(t *testing.T) {
	m, err := saneMoveFromFen("rnbqk2r/pppp1ppp/8/2b1p3/3Pn3/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 0 1")
	if err != nil {
		t.Error(err)
	}

	if m.String() != "c3e4" {
		t.Errorf("Did not get expected move c3e4. got %s", m)
	}
}

func Test_blackMove(t *testing.T) {
	m, err := saneMoveFromFen("rnbqk2r/pppp1ppp/8/2P1p3/4n3/2N2N2/PPP2PPP/R1BQKB1R b KQkq - 0 1")
	if err != nil {
		t.Error(err)
	}
	expected := "e4c3"
	got := fmt.Sprintf("%s%s", m.From, m.To)
	if got != expected {
		t.Errorf("got %s, expected %s", got, expected)
	}
}

func AnotherBlackMove(t *testing.T) {
	m, err := saneMoveFromFen("rnb1kbr1/ppp1n2p/3P1P2/3P4/6Q1/2N5/PPP2P1P/R1B1KBNR b KQq - 0 10")
	if err != nil {
		t.Error(err)
	}
	expected := "c8g4"
	got := fmt.Sprintf("%s%s", m.From, m.To)
	if got != expected {
		t.Errorf("got %s, expected %s", got, expected)
	}
}

/// perf. i nettlesern klarer vi 7500 pos. pr sek.
//  test viser BenchmarkEval-4       3000    371816 ns/op ( git e863ade )

func BenchmarkEvaluateFEN(b *testing.B) {
	for i := 0; i < b.N; i++ {
		got, err := evaluateFEN("rnbqkbnr/ppppppp1/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
		if err != nil {
			b.Error(err)
		}
		if got != 10 {
			b.Error("expected 10, got", got)
		}
	}
}

// 1 000 000 000/10 861 ns/op = 92 000 pr sek. ( git e863ade )
func BenchmarkEvaluateBoard(b *testing.B) {

	g, e := gameAfterMove("rnbqkbnr/ppppppp1/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", "")
	if e != nil {
		b.Error(e)
		return
	}
	board := g.Position().Board()
	for i := 0; i < b.N; i++ {
		got := evaluateBoard(board)
		if got != 10 {
			b.Error("expected 10, got", got)
		}
	}
}

// 282 226 034 ns/op ( git e863ade )
//   3 477 851 ns/op ( using clone after b7ae652)
//     545 835 ns/op ( using positions, not game.)
func BenchmarkSaneMove(b *testing.B) {
	fen, err := chess.FEN("rnbqk2r/pppp1ppp/8/2P1p3/4n3/2N2N2/PPP2PPP/R1BQKB1R b KQkq - 0 1")
	if err != nil {
		b.Error("err")
	}
	game := chess.NewGame(chess.UseNotation(chess.LongAlgebraicNotation{}), fen)
	if game.Outcome() != chess.NoOutcome {
		b.Error("Game has outcome")
	}

	p := game.Position()
	for i := 0; i < b.N; i++ {
		bestMove, bestScore := getSaneMove(p)
		expected := "e4c3"
		if bestMove.String() != expected {
			b.Errorf("got %s, expected %s", bestMove, expected)
		}
		if bestScore != -10 {
			b.Error("Expected other score. got", bestScore)
		}
	}
}
